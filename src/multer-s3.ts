import {S3Client} from '@aws-sdk/client-s3';
import {Request} from 'express';
import {StorageEngine} from 'multer';
import {S3UploadStream} from './s3-upload-stream';

export interface MulterS3File {
  url: string;
  etag: string;
}

export class MulterS3 implements StorageEngine {
  constructor(
    private readonly opts: {
      s3Client: S3Client;
      bucket: string;
      key: string | ((fileName: string) => string);
    }
  ) {}

  _handleFile(
    req: Request,
    file: Express.Multer.File,
    callback: (
      error?: Error | null,
      info?: (Partial<Express.Multer.File> & MulterS3File) | undefined
    ) => void
  ): void {
    let s3MetaInfo: {url: string; etag: string};

    const key: string =
      typeof this.opts.key === 'string'
        ? this.opts.key
        : this.opts.key(file.originalname);

    file.stream
      .pipe(
        new S3UploadStream({
          s3Client: this.opts.s3Client,
          bucket: this.opts.bucket,
          key,
          fileSize: file.size,
        })
      )
      .on('s3-meta', meta => {
        //console.log('s3-meta:', meta);
        s3MetaInfo = meta;
      })
      .on('error', error => {
        //console.log('error:', error);
        callback(error);
      })
      .on('finish', () => {
        //console.log('finish');
        callback(null, s3MetaInfo);
      })
      .resume();
  }

  _removeFile(
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error | null) => void
  ): void {
    console.log('_removeFile');
    callback(null);
  }
}
