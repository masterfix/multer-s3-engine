import {PutObjectCommand, S3Client} from '@aws-sdk/client-s3';
import {PassThrough, Transform, TransformCallback} from 'stream';

export type S3MetaData = {
  etag: string;
  url: string;
};

export class S3UploadStream extends Transform {
  private passThrough: PassThrough;

  private sendStarted = false;

  private finalCallback?: (error?: Error | null | undefined) => void;

  constructor(
    private readonly opts: {
      s3Client: S3Client;
      bucket: string;
      key: string;
      fileSize: number;
    }
  ) {
    super();

    this.passThrough = new PassThrough();
  }

  _transform(
    chunk: unknown,
    encoding: BufferEncoding,
    callback: TransformCallback
  ): void {
    if (!this.sendStarted) {
      this.setupS3Command();
    }

    const writeResult = this.passThrough.write(chunk, encoding);

    //console.log('_write chunk:', chunk);
    if (writeResult) {
      this.push(chunk, encoding);
      callback();
    } else {
      this.passThrough.once('drain', () => {
        //console.log('drain event');
        this.push(chunk, encoding);
        callback();
      });
      //console.log('writeResult is false!');
    }
  }

  _final(callback: (error?: Error | null | undefined) => void): void {
    //console.log('final');

    this.passThrough.end();

    this.finalCallback = callback;
  }

  private setupS3Command() {
    //console.log('setupS3Command');

    const putObjectCommand = new PutObjectCommand({
      Bucket: this.opts.bucket,
      Key: this.opts.key,
      ACL: 'public-read',
      Body: this.passThrough,
      ContentLength: this.opts.fileSize,
    });

    this.opts.s3Client.send(putObjectCommand, (error, data) => {
      //console.log('s3 putObjectCommand finished, error:', error, 'data:', data);

      if (error) {
        //console.log('s3 error:', error);
        this.destroy(error);
        return;
      }

      if (data) {
        this.emit('s3-meta', {
          etag: data.ETag,
          url: `s3://${this.opts.bucket}/${this.opts.key}`,
        } as S3MetaData);
      } else {
        console.log('no s3 data received');
      }

      this.push(null);

      if (this.finalCallback) {
        this.finalCallback();
      } else {
        console.log('finalCallback is not set');
      }
    });

    this.sendStarted = true;
  }
}
