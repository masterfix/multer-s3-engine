import {S3Client} from '@aws-sdk/client-s3';
import {Request} from 'express';
import {createReadStream, statSync} from 'fs';
import {MulterS3, MulterS3File} from '../src/multer-s3';
import path = require('path');

describe('MulterS3', () => {
  let s3Client: S3Client;

  let multerS3: MulterS3;

  const inputFilePath = 'test/files/acl-2.3.1-18-x86_64.pkg.tar.gz';
  const inputFileName = path.basename(inputFilePath);
  const fileStat = statSync(inputFilePath);

  let mockRequest: Partial<Request>;
  let mockFile: Partial<Express.Multer.File>;

  beforeEach(() => {
    s3Client = new S3Client({
      endpoint: 'https://eu-central-1.linodeobjects.com',
      region: 'US',
      credentials: {
        accessKeyId: 'EJ402DK9FMMO8TVIMPZA',
        secretAccessKey: 'xvkKEB5m7gP3c66cIt7nE974unDJpvNxe0bGCCKx',
      },
    });

    multerS3 = new MulterS3({
      s3Client,
      bucket: 'multer-s3-storage',
      key: fileName => `test/${fileName}`,
    });

    mockRequest = {};
    mockFile = {
      stream: createReadStream(inputFilePath),
      size: fileStat.size,
      originalname: inputFileName,
    };
  });

  it('should create instance', () => {
    expect(multerS3).toBeInstanceOf(MulterS3);
  });

  it('should upload a file correctly', () => {
    return new Promise<void>(resolve => {
      multerS3._handleFile(
        mockRequest as Request,
        mockFile as Express.Multer.File,
        (error: unknown, info?: MulterS3File) => {
          expect(error).toBeNull();
          expect(info?.url).toBe(
            `s3://multer-s3-storage/test/${inputFileName}`
          );
          resolve();
        }
      );
    });
  }, 20000);

  it('should accept a string as key', () => {
    return new Promise<void>(resolve => {
      multerS3 = new MulterS3({
        s3Client,
        bucket: 'multer-s3-storage',
        key: 'bla/XXX',
      });

      multerS3._handleFile(
        mockRequest as Request,
        mockFile as Express.Multer.File,
        (error: unknown, info?: MulterS3File) => {
          expect(error).toBeNull();
          expect(info?.url).toBe('s3://multer-s3-storage/bla/XXX');
          resolve();
        }
      );
    });
  }, 20000);

  it('should accept a function as key', () => {
    return new Promise<void>(resolve => {
      multerS3 = new MulterS3({
        s3Client,
        bucket: 'multer-s3-storage',
        key: fileName => `test2/${fileName}`,
      });

      multerS3._handleFile(
        mockRequest as Request,
        mockFile as Express.Multer.File,
        (error: unknown, info?: MulterS3File) => {
          expect(error).toBeNull();
          expect(info?.url).toBe(
            `s3://multer-s3-storage/test2/${inputFileName}`
          );
          resolve();
        }
      );
    });
  }, 20000);
});
